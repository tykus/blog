echo "Updating packages"
sudo apt-get update

echo "Installing cURL"
sudo apt-get install curl

echo "Installing RVM and latest Ruby version"
curl -L https://get.rvm.io | bash -s stable --ruby

echo "Reloading profile"
source ~/.profile

echo "Adding RVM to profile"
echo '[[ -s "$HOME/.rvm/scripts/rvm" ]] && . "$HOME/.rvm/scripts/rvm"' >> ~/.profile

echo "Installing Rails"
gem install --no-rdoc --no-ri rails

echo "Setting up NodeJS"
curl -sL https://deb.nodesource.com/setup | sudo bash -
sudo apt-get install nodejs